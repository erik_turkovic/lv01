package com.rplv;

public class zadatak5 {
    public static class avion{
        public void fly(){
            System.out.println("Avion leti");
        }
        public void fly(Integer visina){
            System.out.println("Avion leti na "+visina+" metara.");
        }
        public void fly(Integer distance, Integer sati){
            System.out.println("Najduži let je bil "+distance+" i trajal je "+sati);
        }
    }
}
