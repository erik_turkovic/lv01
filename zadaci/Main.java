package com.rplv;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {

        abstract class student {




            public void intro(){
                System.out.println("studentski zivot");
            }

            public void sleep(){
            }

            public void  learn(){
            }

        }

        class GoodStudent extends student{
            public void intro(){
                System.out.println("studentski zivot");
            }

            public void sleep(){
                System.out.println("spi dosta");
            }

            public void  learn(){
                System.out.println("Uči puno");
            }

        }
        class BadStudent extends student{
            public void intro(){
                System.out.println("studentski zivot");
            }

            public void sleep(){
                System.out.println("prevec spi");
            }

            public void  learn(){
                System.out.println("ne uci");
            }

        }
//ZADATAK 1
        GoodStudent a = new GoodStudent();
        BadStudent b = new BadStudent();
        a.learn();
        b.learn();
        a.intro();
        b.intro();
        a.sleep();
        b.sleep();
//ZADATAK 2
        zadatak2.engineeringstudent c = new zadatak2.engineeringstudent();
        b.sleep();
        c.PrintYearAndName();
        c.sleep();
        c.WatchTV();

//ZADATAK 3
        zadatak3.Student d = new zadatak3.Student();
        d.setgodina(3);
        d.setname("Mit");
        d.setprosjeka(2.3);
        System.out.println(d.getname(d.name));
        System.out.println(d.getgodina(d.godina));
        System.out.println(d.getprosjeka(d.prosjek));

//ZADATAK 4
        zadatak4.Car ferrari = new zadatak4.Car();
        System.out.println("Boja: "+ferrari.Color + ", Vrata: " + ferrari.doors + ", wheels: "+ferrari.wheels+ ", engine: "+ ferrari.EngineType);

//ZADATAK 5
        zadatak5.avion boing = new zadatak5.avion();
        boing.fly();
        boing.fly(1000);
        boing.fly(2000, 10);

    }
}
