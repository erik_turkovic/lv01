package com.rplv;

import java.math.BigDecimal;

public class zadatak3 {
    public static class Student{
        String name;
        Integer godina;
        Double prosjek;


        public void setname(String name){
            this.name =  name;
        }
        public void setgodina(Integer godina){
            this.godina =  godina;
        }
        public void setprosjeka(Double prosjek){
            this.prosjek =  prosjek;
        }
        public String getname(String name){
            return this.name;
        }
        public Integer getgodina(Integer godina){
            return this.godina;
        }
        public Double getprosjeka(Double prosjek) {
            return this.prosjek;
        }

    }
}
